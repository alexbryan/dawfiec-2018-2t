from rest_framework import routers
from accounts import api_views as myapp_views

router = routers.DefaultRouter()

router.register(r'usuarios',myapp_views.UserViewset)
router.register(r'servicios',myapp_views.ServiceViewset)
router.register(r'citas',myapp_views.AppointmentViewset)