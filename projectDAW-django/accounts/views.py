from django.contrib.auth import login as auth_login
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from .controller import ListarServicio,ListarCita, Add, DeleteCita
from django.utils.dateparse import parse_date
from django.urls import reverse_lazy, reverse
from .forms import SignUpForm, PostForm, CreateServiceForm
from index.models import Appointment, Service
from reportes.models import *
from reportes.serializers import *
from datetime import datetime

from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            auth_login(request, user)
            return redirect('login')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


def userprofile(request):
    listaservicio = ListarServicio('servicios')
    context = {'service': listaservicio}
    return render(request, 'services.html', context)


def ListarCitas(request):
    listacitas = ListarCita('citas')
    context = {'citas':listacitas}
    return render(request, 'horarios.html', context)


class ServiceCreate(CreateView):
    model = Service    
    form_class = CreateServiceForm
    template_name = 'service_create.html'
    success_url = reverse_lazy('userprofile')


def deleteCita(request, dato):
	DeleteCita(dato)
	return redirect('citas')


class UserUpdateView(UpdateView):
    model = User
    fields = ('first_name', 'last_name', 'email', 'username',)
    template_name = 'my_account.html'
    success_url = reverse_lazy('my_account')

    def get_object(self):
        return self.request.user

class CitaCreate(CreateView):
    model = Appointment
    fields = ('date', 'service', 'id_user')
    template_name = '.html'
    success_url = reverse_lazy('my_account')

    def get_object(self):
        return self.request.user

def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('change_password')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'change_password.html', {
        'form': form
    })

def post_new(request, service, user):
    if request.method == "POST":
        form = PostForm(request.POST)
        date = datetime.now().isoformat()
        registro = {'date': date, 'service': int(service), 'id_user': int(user)}
        print(registro)
        Add(registro)
        print(registro)    
        return redirect('userprofile')
    else:
        #return redirect('citas')
        form = PostForm()
    return render(request, 'add.html', {'form': form})
    #return render(request, 'add.html')


def addService(request):
    if request.method == 'POST':
        form = CreateServiceForm(request.POST)
        service = form.save()
        print(service)
        #registro = {'description': form., 'tipo': , 'price': , 'is_active':)}
        #Add(registro)
        #return redirect('userprofile')
    else:
        form = CreateServiceForm()
    return render(request, 'service_create.html', {'form': form})