from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from index.models import Appointment, Service

class SignUpForm(UserCreationForm):
    email = forms.CharField(max_length = 254, required = True, widget = forms.EmailInput())
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

class CreateServiceForm(forms.ModelForm):

    class Meta:
        model = Service
        fields = ['description', 'tipo', 'price', 'is_active']


class EditProfileForm(UserChangeForm):
    template_name='my_account.html'

    class Meta:
        model = User
        fields = (
            'email',
            'first_name',
            'last_name',
            'password'
        )

class PostForm(forms.ModelForm):

    class Meta:
        model = Appointment
        fields = ['date']
        #widgets = {'service': forms.HiddenInput()}
