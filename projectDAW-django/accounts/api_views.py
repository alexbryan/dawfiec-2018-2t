from rest_framework import viewsets
from index import models as im
from . import serializers as s
from django.contrib.auth.models import User
from rest_framework.views import APIView

class UserViewset(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = s.UserSerializer

class ServiceViewset(viewsets.ModelViewSet):
    queryset = im.Service.objects.all()
    serializer_class = s.ServiceSerializer

class AppointmentViewset(viewsets.ModelViewSet):
    queryset = im.Appointment.objects.all()
    serializer_class = s.AppointmentSerializer   

