from rest_framework_mongoengine import viewsets
from .models import CategoriaReporte
from .serializers import CategoriaReporteSerializer

class CategoriaReporteViewSet(viewsets.ModelViewSet):
    lookup_field = 'id'
    serializer_class = CategoriaReporteSerializer
    def get_queryset(self):
        return CategoriaReporte.objects.all()