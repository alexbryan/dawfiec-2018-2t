from rest_framework_mongoengine import serializers
from .models import *

class CategoriaReporteSerializer(serializers.DocumentSerializer):
    class Meta:
        model = CategoriaReporte
        fields = '__all__'
