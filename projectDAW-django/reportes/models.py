from djongo import models

class CategoriaReporte(models.Model):
	nombreCategoria = models.CharField(max_length=30)
	usuarioId = models.IntegerField()
	
	class Meta:
		app_label = 'reportes'