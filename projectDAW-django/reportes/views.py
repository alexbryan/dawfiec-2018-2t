from django_datatables_view.base_datatable_view import BaseDatatableView
from django.utils.html import escape
from index.models import Service
from django.contrib.auth.models import User
from .controller import listadoServicios
from django.shortcuts import render
from accounts.controller import ListarServicio


from .models import CategoriaReporte 


def listarServicios(request):
    model = ListarServicio('servicios')
    categoriaReporte = CategoriaReporte.objects.all() 
    lista = []
    for c in categoriaReporte:
        registro = {'id': c.id, 'nombreCategoria': c.nombreCategoria, 'usuarioId': c.usuarioId}
        lista.append(registro)
    context = {'object_list': model, 'object_list2': lista}
    return render(request, 'report_services.html', context)