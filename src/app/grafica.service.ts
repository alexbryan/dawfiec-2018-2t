import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators"


@Injectable({
  providedIn: 'root'
})
export class GraficaService {
  apiKey: string = ""

  constructor(private http: HttpClient) { }

  graficoEstadistico(): Observable<any> {
    return this.http.get(`http://data.fixer.io/api/latest?access_key=a447e296366db1a1f97dbea76cdd4a45`)
  }
}
