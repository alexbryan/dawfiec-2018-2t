import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { GraficaService } from './grafica.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  suscripcion: Subscription

  chart = []

  constructor(private graficaService: GraficaService) { }

  ngOnInit() {
    this.suscripcion = this.graficaService.graficoEstadistico()
      .subscribe(
        data => {
          const rates = data.rates
          let etiquetas = []
          let tasas = []

          for (let rate in rates) {
            etiquetas.push(rate)
            tasas.push(rates[rate])
          }

          etiquetas = etiquetas.slice(0, 15)
          tasas = tasas.slice(0, 15)

          this.chart = new Chart('g1', {
            type: 'bar',
            data: {
              labels: etiquetas,
              datasets: [
                {
                  label: "Tasa",
                  data: tasas,
                  borderColor: '#3cba9f',
                  fill: false
                }
              ]
            },
            options: {
              legend: {
                display: false
              },
              scales: {
                xAxes: [{
                  display: true
                }],
                yAxes: [{
                  display: true
                }]
              }
            }
          });



          this.chart = new Chart('g2', {
            type: 'pie',
            data: {
              labels: etiquetas,
              datasets: [
                {
                  label: "Tasa",
                  data: tasas,
                  borderColor: '#3cba9f',
                  fill: false
                }
              ]
            },
            options: {
              legend: {
                display: false
              },
              scales: {
                xAxes: [{
                  display: true
                }],
                yAxes: [{
                  display: true
                }]
              }
            }
          });



          this.chart = new Chart('g3', {
            type: 'line',
            data: {
              labels: etiquetas,
              datasets: [
                {
                  label: "Tasa",
                  data: tasas,
                  borderColor: '#3cba9f',
                  fill: false
                }
              ]
            },
            options: {
              legend: {
                display: false
              },
              scales: {
                xAxes: [{
                  display: true
                }],
                yAxes: [{
                  display: true
                }]
              }
            }
          });







          


        }
      )
  }

  ngOnDestroy() {
    this.suscripcion.unsubscribe()
  }
}
